import json

import click

import client


@click.command()
@click.option(
    "--type_param",
    help="Type of value.",
    type=click.Choice(["int", "char", "string"], case_sensitive=False),
    prompt="Type",
    required=True,
)
@click.option("--value_param", help="Value", prompt="Value", required=True)
@click.option(
    "--host",
    help="The server host",
    prompt="Server host",
    default="172.17.0.2",
    required=False,
)
@click.option(
    "--port", help="The server port", prompt="Server port", default=8080, required=False
)
def main(type_param, value_param, host, port):

    if type_param.lower() == "int":
        value_param = int(value_param)

    message_json = get_message(type_param, value_param)
    client.start(message_json, host, port)


def get_message(type_param: str, value_param: int or str) -> json:
    """_summary_

    Args:
        type_param:
        value_param:

    Returns:
       return the json message
    """
    return json.dumps({"type": type_param, "value": value_param})


if __name__ == "__main__":

    main()
