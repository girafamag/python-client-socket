import json
import socket
import time


def start(message: json, host: str, port: int):
    """
    This function create a TCP/IP socket, connect with the server,
    send to server the message parameter and print the server response
    Args:
        message: message that will be send to server
        host: server host
        port: server port
    """

    # The first parameter AF_INET is an address family that is used to designate the type of addresses that this socket can communicate with(IPV4)
    # The second parameter SOCK_STREAM is the socket kind
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    print("Connecting to {} port {}.".format(host, port))
    try:
        sock.connect((host, port))
        print("Connected with success.")
    except socket.error as e:
        print("Socket error: {}".format(str(e)))

    try:
        send_data(sock, message)
    except socket.error as e:
        print("Socket error: {}".format(str(e)))
    except Exception as e:
        print("Exception: {}".format(str(e)))
    finally:
        print("Closing the server connection.")
        sock.close()


def send_data(sock: socket, message: json):
    """
    Send the message to server, calculate de RTT and print the server response

    Args:
        sock: server client connected with server
        message: client message to server
    """
    print(f"Sending {message}")

    start = time.time_ns()
    sock.sendall(message.encode("utf-8"))
    data = sock.recv(2048)
    end = time.time_ns()

    print(f"RTT = {end-start} ns.")
    print(f"Server response: {json.loads(data)}")
