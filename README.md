# Python Client Socket

This is a TCP socket project for Computer Network class from UERJ.

## Install requirements
```
>> pip install -r requirements.txt
``` 

## How run locally?

In the terminal you must run the main.py file. To know the required arguments you can use --help. If you don't pass the arguments, you can enter the values at the prompt.
```
>> python3 main.py --help
Usage: main.py [OPTIONS]

Options:
  --type_param [int|char|string]  Type of value.  [required]
  --value_param TEXT              Value  [required]
  --host TEXT                     The server host
  --port INTEGER                  The server port
  --help                          Show this message and exit.

>> python3 main.py --type_param int --value_param 4 --host 172.17.0.2 --port 8080
Connecting to 172.17.0.2 port 8080

>> python3 main.py
Type (int, char, string): int
Value: 4
Server host [172.17.0.2]: 
Server port [8080]: 
Connecting to 172.17.0.2 port 8080
```